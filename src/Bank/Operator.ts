import Receipt from "./Receipt";
import AtmCard from "../AtmCard";

export default abstract class Operator {
    private orderId:number;
    protected handingCharge:number;
    constructor(handingCharge:number) {
        this.orderId = 0;
        this.handingCharge = handingCharge;
    }
    
    abstract doWithDraw(card:AtmCard, money:number):Receipt;
    abstract doDeposit(card:AtmCard, money:number):Receipt;
    protected getNextOrderId():number {
        return ++this.orderId;
    }

    protected prefixInteger(num:number, length:number):string {
        return (Array(length).join('0') + num).slice(-length);
    }
}