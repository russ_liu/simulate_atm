export default class Receipt {
    private orderId:string;
    private message:string;
    private status:boolean;
    constructor(orderId:string, message:string, status:boolean) {
        this.orderId = orderId;
        this.message = message;
        this.status = status;
    }

    public getContext():string {
        return `訂單編號:${this.orderId},\n${this.message},\n交易結果:${this.status?'成功':'失敗'}`;
    }

    public getStatus():boolean {
        return this.status;
    }
}