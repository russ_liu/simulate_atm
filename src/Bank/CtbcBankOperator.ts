import Receipt from "./Receipt";
import AtmCard from "../AtmCard";
import Operator from "./Operator";
import { BankCode } from "BankCode";

export default class CtbcBankOperator extends Operator {
    constructor() {
        super(15); // 手續費15元
    }

    doWithDraw(card: AtmCard, money: number): Receipt {
        let extratCharge = card.getCode() == BankCode.CTBC ? 0 : 5;
        return new Receipt(
                this.prefixInteger(this.getNextOrderId(), 5),
                `從${card.getAccount()}提領:${money}元, 手續費:${this.handingCharge}元, 跨行轉帳手續費${extratCharge}元`,
                true);
    }
    doDeposit(card: AtmCard, money: number): Receipt {
        throw new Error("Method not implemented.");
    }
}