import * as readline from 'readline';
import ATM from './ATM';
import AtmCard from './AtmCard';
import { BankCodeMap } from './BankCode';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

async function ask(question:string) {
    return new Promise<string>((resolve, reject) => {
        rl.question(question, (input:string) => resolve(input) );
    });
}

(async () => {
    let atm:any; //ATM.ts
    let money:number = 0;
    let card:AtmCard = new AtmCard();
    let answer;
    console.log(`請輸入的ATM卡片訊息..`);

    answer = await ask('輸入您的帳號所屬銀行(1:台新, 2:玉山, 3:中信):\n');
    card.setCode(BankCodeMap[+answer]);
    if (!card.getCode()) {
        console.log(`未選擇可用銀行... abort.`);
        process.exit();
    }

    answer = await ask('輸入您的銀行帳號:\n');
    if (!answer) {
            console.log(`未輸入銀行帳號... abort.`);
            process.exit();
    } else {
        card.setAccount(answer);
    }

    answer = await ask('輸入您的帳號戶名:\n');
    if (!answer) {
        console.log(`未輸入帳號戶名... abort.`);
        process.exit();
    } else {
        card.setAccount(answer);
    }

    answer = await ask('輸入提款金額:\n');
    if (!answer) {
        console.log(`未輸入提款金額... abort.`);
        process.exit();
    } else {
        money = +answer; // simply convert string to number
    }

    answer = await ask('選擇你想使用哪家銀行的ATM(1:台新, 2:玉山, 3:中信):\n');
    if (!BankCodeMap[+answer]) {
        console.log(`未選擇可用銀行ATM... abort.`);
        process.exit();
    }
    atm = new ATM(BankCodeMap[+answer]);

    atm.withDraw(card, '', money);
    process.exit();
})();

process.on('unhandledRejection', (err) => {
    console.error(err);
    process.exit(-1);
});