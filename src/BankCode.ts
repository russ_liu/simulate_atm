export const enum BankCode {
    TS = 812, // 台新銀行
    E_SUN = 808, // 玉山銀行
    CTBC = 822 //中國信託
}

export const BankCodeMap:Record<number,BankCode> = {
    [1]:BankCode.TS,
    [2]:BankCode.E_SUN,
    [3]:BankCode.CTBC
};