import { BankCode } from "BankCode";
import CtbcBankOperator from "./Bank/CtbcBankOperator";
import EsunBankOperator from "./Bank/EsunBankOperator";
import Operator from "./Bank/Operator";
import TsBankOperator from "./Bank/TsBankOperator";

export default class OperatorFactory {
    public static create(code:BankCode):Operator {
        let rtn:Operator;
        switch (code) {
            case BankCode.TS:
                rtn = new TsBankOperator();
                break;
            case BankCode.E_SUN:
                rtn = new EsunBankOperator();
                break;
            case BankCode.CTBC:
                rtn = new CtbcBankOperator();
                break;
            default:
                throw new Error("Unrecognized BankCode");
        }
        return rtn;
    }
}