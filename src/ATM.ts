import AtmCard from "./AtmCard";
import Operator from "./Bank/Operator";
import OperatorFactory from "./OperatorFactory";
import Receipt from "./Bank/Receipt";
import { BankCode } from "./BankCode";

export default class ATM {
    private cashBox:number;
    private operator:Operator; //屬於哪家銀行作業系統

    public constructor(bankCode:BankCode) {
        this.cashBox = 200000;
        this.operator = OperatorFactory.create(bankCode);
    }

    public withDraw(card:AtmCard, password:string, money:number):void {
        let receipt:Receipt;
        
        if(money > this.cashBox) {
            receipt = new Receipt('-1', `ATM餘額不足`, false)
        } else {
            console.log(`驗證帳戶密碼中...`);
            // 暫時不實作密碼驗證
            console.log(`驗證成功!!!`);
            console.log('銀行系統開始處裡中...');
            receipt = this.operator.doWithDraw(card, money);
        }

        if (receipt.getStatus()) {
            this.cashBox -= money;
        }
        console.log('\n\n\n');
        console.log(receipt.getContext());
    }
}