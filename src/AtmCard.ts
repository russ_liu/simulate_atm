import { BankCode } from "BankCode";

export default class AtmCard {
    public code:BankCode;
    public account:string;
    public username:string;

    constructor();
    public constructor( code?:BankCode,
                        account?:string,
                        username?:string) {
        this.code = code!;
        this.account = account!;
        this.username = username!;
    }

    public getCode():BankCode {
        return this.code;
    }

    public getAccount():string {
        return this.account;
    }

    public getUserName():string {
        return this.username;
    }

    public setCode(code:BankCode):void {
        this.code = code;
    }

    public setAccount(account:string):void {
        this.account = account;
    }

    public setUserName(username:string):void {
        this.username = username;
    }
}