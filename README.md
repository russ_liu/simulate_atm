# 模擬銀行ATM運作練習

## 題目

實作提款機內提款行為,至少要有三家銀行 ( 中信,玉山...etc)  
event Withdraw  
input :bankcode 銀行代碼,userid 使用者ID ,withdrawamount 提款金額  
output: status 狀態 ,message 狀態訊息 ,orderId 訂單編號  

不限定任何語言
描述:在提供的Withdraw的方法中實作出各家銀行的提款動作

範例:
```javascript
var withdraw =function(withdrawAmount,id ,bankcode){

    if(bankcode==822){
    
      return {statu:1,message:"sucess",orderId:001}
    }

    if(bankcode==830){
    
      return {statu:1,message:"sucess",orderId:001}
    }
  
};

console.log(withdraw(100,'A1234567',822));
```

## 我的解答
將ATM針對不同銀行定義對應的**Operator class**，並實作各自的doWithDraw。
同時，也使用了Factory來讓ATM class選擇各自要使用的Operator。

- class diagram
```plantuml
enum BankCode {
    TS = 801,   <b><color:green>   // 台新銀行</color>
    ESUN = 812, <b><color:green> // 玉山銀行</color>
    CTBC = 822  <b><color:green> // 中信銀行</color>
}

class AtmCard {
    + code:BankCode
    + account:string
    + userName:string
}

class ATM {
    - operator:Operator
    - cashBox:number
    + constructor(bankCode:BankCode)
    + withDraw(card:AtmCard, password:string, money:number):void
    + deposit(card:AtmCard, password:string, money:number):void
}

class OperatorFactory {
    + create(code: BankCode): Operator
}

namespace bank #DDDDDD {
    abstract class Operator {
        - orderId:number
        ~ handingCharge:number
        ~ constructor(handingCharge:number)
        ~ getNextOrderId():number;
        + {abstract}doWithDraw(card:AtmCard, money:number):Receipt
        + {abstract}doDeposit(card:AtmCard, money:number):Receipt
    }

    class TsBankOperator extends Operator {
        + doWithDraw(card:AtmCard, money:number):Receipt
        + doDeposit(card:AtmCard, money:number):Receipt
    }
    class EsunBankOperator extends Operator {
        + doWithDraw(card:AtmCard, money:number):Receipt
        + doDeposit(card:AtmCard, money:number):Receipt
    }
    class CtbcBankOperator extends Operator {
        + doWithDraw(card:AtmCard, money:number):Receipt
        + doDeposit(card:AtmCard, money:number):Receipt
    }

    class Receipt {
        - orderId:string
        - message:string
        + status:boolean
        + getContext():string
    }

    bank.Operator -right-> Receipt:create
}
ATM -right-> OperatorFactory:use
OperatorFactory --> bank.Operator:create
```
